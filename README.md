This repository contains the necessary files for the execution of USB communication practices on the STM32F411E Discovery card using the CUBE HAL library.

The most appropriate way to create a new project in the "System Workbench" development environment is as follows:

  Create your C project.
  
  In a new C project, in the "Project Firmware configuration" option, select the option "Add low lever drivers in the project" and ADD "STM32_USB_Device_Library". With this option, the necessary files for the initialization of USB peripherals are created.

  At the address: NAME_OF_PROJECT/HAL_Driver, you will find: Inc, Src, Release_Notes.html, etc .; these files are replaced with the files available in this repository.

  At the address: NAME_OF_PROJECT/Middlewares/ the only thing available should be:
     ST / STM32_USB_Device_Library / Class / CDC / Inc / usbd_cdc.h
     ST / STM32_USB_Device_Library Class / CDC / Src / usbd_cdc.c
  THE REST OF FILES SHOULD BE ELIMINATED.

  At the address: NAME_OF_PROJECT/inc and NAME_OF_PROJECT/src, replace the original files with those available in this repository.
With these steps, the project can be compiled correctly.

  at the file main.c:
  
  At the beginning of the main program the HAL_Init() function is called to reset all the peripherals, initialize the Flash interface and the systick.
  
  Then the SystemClock_Config() function is used to configure the system clock (SYSCLK) to run at 84 MHz.
