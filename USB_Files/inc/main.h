/*
 * main.h
 *
 *  Created on: Nov 12, 2017
 *      Author: pablobuestan
 */

#ifndef MAIN_H_
#define MAIN_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f411e_discovery.h"
#include "stm32f4xx.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "usbd_desc.h"

void SystemClock_Config(void);
void Error_Handler(void);
uint32_t USBConfig(void);

#endif /* MAIN_H_ */
