#include "main.h"

//char print_message [100];

int main(void)
{
  HAL_Init();
  SystemClock_Config();
  USBConfig();
  USBD_CDC_ReceivePacket(&hUSBDDevice);
  while (1)
  {
    //sprintf (print_message,"Hello World!");
    //USBD_CDC_SetTxBuffer(&hUSBDDevice,print_message,100);
    //USBD_CDC_TransmitPacket(&hUSBDDevice);
  }
}

void LSE_Receive_callback(USBD_HandleTypeDef* husbd, uint8_t* Buf, uint32_t *len) 
{
  BSP_LED_Toggle(LED3);
  if (*len > 0) 
  {
    //Program here what happen when the user press one key...
  }
  USBD_CDC_ReceivePacket(&hUSBDDevice);
}


uint32_t USBConfig(void)
{
  /* Init Device Library */
  USBD_Init(&hUSBDDevice, &CDC_Desc, 0);
  /* Add Supported Class */
  USBD_RegisterClass(&hUSBDDevice, USBD_CDC_CLASS);
  /* Register user interface */
  USBD_CDC_RegisterInterface(&hUSBDDevice, &USBD_CDC_LSE_fops);
  /* Start Device Process */
  USBD_Start(&hUSBDDevice);
  return 0;
}

void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
	  Error_Handler();
  }
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
	  Error_Handler();
  }
}

void Error_Handler(void)
{
  /* User may add here some code to deal with this error */
  while(1)
  {

  }
}
